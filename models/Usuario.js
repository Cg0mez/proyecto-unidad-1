var mongoose = require("mongoose");
var Reserva = require("./Reserva");
var uniqueValidator = require("mongoose-unique-validator");
var Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const crypto = require("crypto");

const mailer = require("../mailer/mailer");
let Token = require("../models/Token");

const saltRounds = 10;

const validateEmail = function (email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    trim: true,
    required: [true, "El email es obligatorio"],
    lowercase: true,
    unique: true,
    validate: [validateEmail, "Por favor ingresar un Email valido"],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/],
  },
  password: {
    type: String,
    required: [true, "El password es obligatorio"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false,
  },
  googleId: String,
  facebookId: String,
});

usuarioSchema.plugin(uniqueValidator, {
  message: "El {PATH} ya existe con otro usuario",
});

usuarioSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function (biciID, desde, hasta, cb) {
  var reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciID,
    desde: desde,
  });

  console.log(reserva);
  reserva.save(cb);
};
usuarioSchema.methods.EnviarEmail = function (cb) {
  const token = new Token({
    _userID: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const emailDestination = this.email;

  token.save(function (err) {
    if (err) {
    }

    const mailOptions = {
      from: "Cgomez.dev@gmail.com",
      to: emailDestination,
      subject: "Account Verification",
      text:
        "Hi, \n\n" +
        "Please, to verify your account, click on the following link:\n\n" +
        "https://bicis-app.herokuapp.com" +
        "/token/confirmation/" +
        token.token,
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return console.log(err.message, mailOptions, "desde sendmail");
      }

      console.log("Email send to " + emailDestination + ".");
    });
  });
};
usuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _userID: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const emailDestination = this.email;
  token.save(function (err) {
    if (err) {
      return cb(err);
    }

    const mailOptions = {
      from: "no-reply@BicycleNetwork.com",
      to: emailDestination,
      subject: "Reseteo de password",
      text:
        "Hi, \n\n" +
        "Please, to reset your account, click on the following link: \n" +
        "http://localhost:3000" +
        "/resetPassword/" +
        token.token +
        ".\n",
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        console.log("error al enviar mail");
        return cb(err);
      }
      console.log(
        "Se envio mail para resetar password a:" + email_destination + "."
      );
    });

    cb(null);
  });
};

//Metodos de AuthO orig

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(
  condition,
  cb
) {
  console.log("ingresa a findOrCreate");
  const self = this;

  self.findOne(
    {
      $or: [{ googleId: condition.id }, { email: condition.emails[0].value }],
    },
    (err, result) => {
      if (result) {
        cb(err, result);
      } else {
        let values = {};
        values.id = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "Sin nombre";
        values.verificado = true;
        values.password = condition.emails[0].value + "pass";

        self.create(values, (err, result) => {
          if (err) {
            console.log(err);
          }
          return cb(err, result);
        });
      }
    }
  );
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(
  condition,
  callback
) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [{ facebookId: condition.id }, { email: condition.emails[0].value }],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("------condition--------");
        console.log(condition);
        let values = {};
        values.facebookId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log("++++++values+++++++++");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) {
            console.log(err);
          }
          return callback(err, result);
        });
      }
    }
  );
};

module.exports = mongoose.model("usuario", usuarioSchema);
