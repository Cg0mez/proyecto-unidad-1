require("newrelic");
require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var passport = require("./config/passport");
var session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const User = require("./models/Usuario");
const Token = require("./models/Token");
const jwt = require("jsonwebtoken");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var tokenRouter = require("./routes/token");
var authAPIRouter = require("./routes/API/auth");

var bicicletasRouter = require("./routes/bicicletas");
var bicicletasAPIRouter = require("./routes/API/bicicletas");
var UsuariosApiRouter = require("./routes/API/usuarios");

let store;

if (process.env.NODE_ENV === "development") {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "sessions",
  });

  store.on("error", function (error) {
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();

app.set("secretKey", "jwt_pwd_!!223344");
app.use(
  session({
    cookie: { maxAge: 240 * 60 * 60 * 1000 },
    store: store,
    saveUninitialized: true,
    resave: "true",
    secret: 'red_bicis_!!!****!".!".!"..1234',
  })
);

var mongoose = require("mongoose");
const { token } = require("morgan");
const { ConsoleReporter } = require("jasmine");
const { assert } = require("console");
//var mongoDB = "mongodb://localhost/red_bicicletas";
var mongoDB = process.env.MONGO_URI;

mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on("error", console.error.bind(console, "mongoDB connecion error: "));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, "public")));
//renderear vistas de login sin pasar por routes
app.get("/login", function (req, res) {
  res.render("sesion/login");
});

app.post("/login", function (req, res, next) {
  passport.authenticate("local", function (err, usuario, info) {
    if (err) return next(err);
    if (!usuario) return res.render("sesion/login", { info });
    req.login(usuario, function (err) {
      if (err) return next(err);
      return res.redirect("/");
    });
  })(req, res, next);
});

// copia funciones resetpasswod
app.get("/forgotPassword", function (req, res) {
  res.render("sesion/forgotPassword");
});

app.post("/forgotPassword", function (req, res) {
  User.findOne({ email: req.body.email }, function (err, user) {
    if (!user)
      return res.render("sesion/forgotPassword", {
        info: { message: "No existe el email para un usuario existente" },
      });

    user.resetPassword(function (err) {
      if (err) return next(err);
      console.log("sesion/forgotPasswordMessage");
    });

    res.render("sesion/forgotPasswordMessage");
  });
});

app.get("/resetPassword/:token", function (req, res, next) {
  console.log(req.params.token);
  Token.findOne({ token: req.params.token }, function (err, token) {
    if (!token)
      return res.status(400).send({
        msg:
          "No existe un usuario asociado al token, verifique que su token no haya expirado",
      });
    User.findById(token._userID, function (err, user) {
      if (!user)
        return res
          .status(400)
          .send({ msg: "No existe un usuario asociado al token." });
      res.render("sesion/resetPassword", { errors: {}, usuario: user });
    });
  });
});

app.post("/resetPassword", function (req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render("sesion/resetPassword", {
      errors: {
        confirm_password: { message: "No coincide con el password ingresado" },
      },
      usuario: new User({ email: req.body.email }),
    });
    return;
  }
  User.findOne({ email: req.body.email }, function (err, user) {
    user.password = req.body.password;
    user.save(function (err) {
      if (err) {
        res.render("sesion/resetPassword", {
          errors: err.errors,
          user: new User({ email: req.body.email }),
        });
      } else {
        res.redirect("/login");
      }
    });
  });
});

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/bicicletas", logedIn, bicicletasRouter);
app.use("/api/bicicletas", validarUsuario, bicicletasAPIRouter);
app.use("/api/usuarios", UsuariosApiRouter);
app.use("/usuarios", logedIn, usersRouter);
app.use("/token", tokenRouter);
app.use("/api/auth", authAPIRouter);
app.use("/privacy-policy", function (req, res) {
  res.sendFile("public/privacy-policy.html");
});
app.use("/google30d1e17422bd4708", function (req, res) {
  res.sendFile("public/google30d1e17422bd4708.html");
});

//Login google
app.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email",
    ],
    accessType: "offline",
    approvalPrompt: "force",
  })
);

app.get(
  "/auth/google/callback",
  passport.authenticate("google", { failureRedirect: "/login" }),
  function (req, res) {
    res.redirect("/");
  }
);

app.get("/logout", function (req, res) {
  req.logOut();
  res.redirect("/");
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

function logedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("user sin loguearse");
    res.redirect("/login");
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(req.headers["x-access-token"], req.app.get("secretKey"), function (
    err,
    decoded
  ) {
    if (err) {
      res.json({ status: "error", message: err.message, data: null });
    } else {
      req.body.userId = decoded.id;

      console.log("jwt verify" + decoded);
      next();
    }
  });
}
/*

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '{your-app-id}',
      cookie     : true,
      xfbml      : true,
      version    : '{api-version}'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

//response


{
    status: 'connected',
    authResponse: {
        accessToken: '...',
        expiresIn:'...',
        signedRequest:'...',
        userID:'...'
    }
}

//devolucion de llamada fb.login
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}
*/
module.exports = app;
