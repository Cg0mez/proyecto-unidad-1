var mymap = L.map("main_map").setView([-34.607744, -58.37189], 13);
497;

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(mymap);

L.marker([-34.59893, -58.380953], { title: "Bici hardcodeada" }).addTo(mymap);

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function (result) {
    console.log(result, "desde ajax");
    result.bicicleta.forEach(function (bici) {
      L.marker(bici.ubicacion, { title: bici.code }).addTo(mymap);
    });
  },
});
