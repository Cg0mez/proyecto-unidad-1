var express = require("express");
var router = express.Router();
var bicicletaController = require("../controllers/bicicletasController");

router.get("/", bicicletaController.bicicletasList);
router.get("/create", bicicletaController.bicicletasCreateGet);
router.post("/create", bicicletaController.bicicletasCreatePost);
router.post("/:id/delete", bicicletaController.bicicletaDeletePost);

router.get("/:id/update", bicicletaController.bicicletaUpdateGet);
router.post("/:id/update", bicicletaController.bicicletaUpdatePost);

module.exports = router;
