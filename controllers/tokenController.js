const User = require("../models/Usuario");
const Token = require("../models/Token");

module.exports = {
  confirmationGet: function (req, res, next) {
    console.log(req.params.token, "desde confirmatios");
    Token.findOne({ token: req.params.token }, function (err, token) {
      console.log(req.params.token + " desde tokenCont");
      console.log(token, "desde token");
      if (!token)
        return res.status(400).send({
          type: "not-verified",
          msg: "No se encontró usuario con ese token",
        });
      User.findById(token._userID, function (err, usuario) {
        if (!usuario)
          return res.status(400).send({ msg: "No se encontró a ese usuario" });
        if (usuario.verified) return res.redirect("/users");
        usuario.verificado = true;
        usuario.save(function (err) {
          if (err) {
            return res.status(500).send({ msg: err.message });
          }
          res.redirect("/");
        });
      });
    });
  },
};
