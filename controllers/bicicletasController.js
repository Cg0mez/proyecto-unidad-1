var bicicleta = require("../models/Bicicleta");

exports.bicicletasList = function (req, res) {
  bicicleta.allBicis(function (error, bicicletas) {
    res.render("bicicletas/index", { bicicletas: bicicletas });
  });
};

exports.bicicletasCreateGet = function (req, res) {
  res.render("bicicletas/create");
};

exports.bicicletasCreatePost = function (req, res) {
  let bici = new bicicleta({
    code: req.body.id,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lng],
  });
  console.log(bici);

  bicicleta.add(bici, function (error, newElement) {
    res.redirect("/bicicletas");
  });
};

exports.bicicletaUpdateGet = function (req, res) {
  bicicleta.findByCode(req.params.id, function (err, bici) {
    console.log(bici);
    res.render("bicicletas/update", { bici });
  });
};

exports.bicicletaUpdatePost = function (req, res) {
  bicicleta.findByCode(req.params.id, function (err, bici) {
    bici.code = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    bici.save();

    res.redirect("/bicicletas");
  });
};

exports.bicicletaDeletePost = function (req, res) {
  bicicleta.removeByCode(req.body.id, function (err) {
    console.log(req.body.id, "desde delete");
    res.redirect("/bicicletas");
  });
};
